def email_parts(email)

	email = email.split("")
	index = 0
	for i in 0..(email.length - 1) do 
		if email[i] == "@"
			index = i
		end
	end

	[email[0..index-1].join(""), email[index+1..(email.length - 1)].join("")]
end