states = ["Kansas", "Nebraska", "North Dakota", "South Dakota"]

def urlify(string)
	string.downcase.split.join("-")
end

def format(states)
	states.map{|state| "https://example.com/#{urlify(state)}"}
end

puts format(states).inspect