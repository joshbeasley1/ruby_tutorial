arr = 1..10

def mult(numbers)
	numbers.reduce(1) {|total, num|  total *= num}
end

puts mult(arr)