states = ["Kansas", "Nebraska", "North Dakota", "South Dakota"]

# lengths: Imperative version
def imperative_lengths(states)
  lengths = {}
  states.each do |state|
    lengths[state] = state.length
  end
  lengths
end
puts imperative_lengths(states)

# lengths: Functional version
def functional_lengths(states)
  states.reduce({}) {|lengths, state| lengths.update(state => state.length)}
end
puts functional_lengths(states)