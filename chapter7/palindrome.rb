# Defines a Phrase class.
class Phrase < String

	def processor(string)
		string.downcase

	private
	def processed_content
		processor(self)
	end

  	def palindrome?
  		processed_content == processed_content.reverse
	end
end

# Defines a translated Phrase.
class TranslatedPhrase < Phrase

	attr_accessor :translation

	def initialize(content, translation)
		super(content)
		@translation = translation
	end

	private
	def processed_content
		processor(translation)
	end
end

