

# Defines a Clock class.
class Clock 
  attr_accessor :hours
  attr_accessor :minutes

  def initialize(hours, minutes)
    additional_hours = minutes / 60
    @hours = (hours + additional_hours) % 24
    @minutes = minutes % 60
  end

  def to_s
  	if self.minutes < 10 and self.hours < 10
    	return "0#{self.hours}:0#{self.minutes}"
    elsif self.minutes < 10
    	return "#{self.hours}:0#{self.minutes}"
    elsif self.hours < 10
    	return "0#{self.hours}:#{self.minutes}"
    else
    	return "#{self.hours}:#{self.minutes}"
    end
  end

  def at(hours, minutes)
  	initialize(hours, minutes)
  	return self
  end

  def +(minutes)
  	self.minutes = self.minutes + minutes
  	initialize(self.hours, self.minutes)
  	return self
  end

  def ==(second_clock)
  	if self.to_s == second_clock.to_s
  		return true
  	else
  		return false
  	end 
  end
end