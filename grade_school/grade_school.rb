# Defines a School class class.
class School 
  attr_accessor :roster

  def initialize
    @roster = []
  end

  def students(grade)
  	if self.roster.length < 1
  		return []
  	end
  	return students[grade][:students]
  end

  def add(name, grade)
  	flag = false
  	if roster.length > 0
	  	self.roster.each do |student| 
	  		if student[:grade] == grade
	  			flag = true
	  			student[:students].push(name)
	  		end
	  	end
	end
  	if not flag
  		names = [name]
  		self.roster.push({grade: grade, students: names})
  	end
  	return self
  end
 
end